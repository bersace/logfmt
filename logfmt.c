#include "postgres.h"
#include <stdio.h>
#include "fmgr.h"
#include "lib/stringinfo.h"
#include "miscadmin.h"
#include "libpq/libpq-be.h"
#include "tcop/tcopprot.h"
#include "utils/elog.h"
#include "utils/memutils.h"
#include "storage/fd.h"

PG_MODULE_MAGIC;

void _PG_init(void);
void _PG_fini(void);

static void write_logfmt(ErrorData *edata);
void appendLogFmtAttr(StringInfoData *buf, const char *key, const char *value);
const char *level_string(int elevel);

static emit_log_hook_type prev_emit_log_hook = NULL;

void
_PG_init(void)
{
	prev_emit_log_hook = emit_log_hook;
	emit_log_hook = write_logfmt;
}

void
_PG_fini(void)
{
	if (emit_log_hook == write_logfmt)
		emit_log_hook = prev_emit_log_hook;
}

static void
write_logfmt(ErrorData *edata)
{
	static bool in_hook = false;
	StringInfoData buf;
	int rc;

	/* Protect from recursive calls */
	if (in_hook)
		return;

	if (!edata->output_to_server)
		return;

	in_hook = true;

	initStringInfo(&buf);
	appendLogFmtAttr(&buf, "ts", get_formatted_log_time());
	reset_formatted_start_time();
	appendLogFmtAttr(&buf, "level", level_string(edata->elevel));
	appendLogFmtAttr(&buf, "message", edata->message);
	appendLogFmtAttr(&buf, "detail", edata->detail);
	appendLogFmtAttr(&buf, "hint", edata->hint);
	appendStringInfoString(&buf, "\n");

	rc = write(fileno(stderr), buf.data, buf.len);

	pfree(buf.data);

	in_hook = false;

	/* Skip built-in writer by skipping error. */
	edata->output_to_server = false;

	return (void)rc;
}

void
appendLogFmtAttr(StringInfoData *buf, const char *key, const char *value)
{
	bool in_quote = false;
	int start;
	int i;

	if (NULL == value)
		return;

	if (0 < strlen(buf->data))
		appendStringInfoChar(buf, ' ');

	appendStringInfoString(buf, key);
	appendStringInfoChar(buf, '=');

	/* value = my log message
	 * start = ^
	 * i     = ^
	 * buf   = key=
	 */
	start = 0;
	for (i = start; value[i] != '\0'; i++) {
		if (' ' == value[i] || '"' == value[i]) {
			if (!in_quote) {
				appendStringInfoChar(buf, '"');
				in_quote = true;
			}

			if (i - start) {
				/* value = my log message
				 * start = ^
				 * i     =   ^
				 * buf   = key=
				 */
				appendBinaryStringInfo(buf, value + start, i - start);
			}
			/* value = my log message
			 * start =   ^
			 * i     =    ^
			 * buf   = key=my
			 */
			start = i + 1;

			if ('"' == value[i]) {
				/* value = my"log message
				 * start =   ^
				 * i     =   ^
				 * buf   = key=my\"
				 */
				appendStringInfoChar(buf, '\\');
				appendStringInfoChar(buf, '"');
			} else {
				appendStringInfoChar(buf, ' ');
			}
		}
	}
	if (i - start)
		appendBinaryStringInfo(buf, value + start, i - start);
	if (in_quote)
		appendStringInfoChar(buf, '"');
}

/* Stolen from elog.c
 */

static struct timeval saved_timeval;
static bool saved_timeval_set = false;

#define FORMATTED_TS_LEN 128
/* static char formatted_start_time[FORMATTED_TS_LEN]; */
static char formatted_log_time[FORMATTED_TS_LEN];

char *
get_formatted_log_time(void)
{
	pg_time_t stamp_time;
	char msbuf[13];

	/* leave if already computed */
	if (formatted_log_time[0] != '\0')
		return formatted_log_time;

	if (!saved_timeval_set) {
		gettimeofday(&saved_timeval, NULL);
		saved_timeval_set = true;
	}

	stamp_time = (pg_time_t)saved_timeval.tv_sec;

	/*
	 * Note: we expect that guc.c will ensure that log_timezone is set up (at
	 * least with a minimal GMT value) before Log_line_prefix can become
	 * nonempty or CSV mode can be selected.
	 */
	pg_strftime(formatted_log_time, FORMATTED_TS_LEN,
		/* leave room for milliseconds... */
		"%Y-%m-%dT%H:%M:%S    %z", pg_localtime(&stamp_time, log_timezone));

	/* 'paste' milliseconds into place... */
	sprintf(msbuf, ".%03d", (int)(saved_timeval.tv_usec / 1000));
	memcpy(formatted_log_time + 19, msbuf, 4);

	return formatted_log_time;
}

/* Stolen from jsonlog.c */
const char *
level_string(int elevel)
{
	const char *s;

	switch (elevel) {
	case DEBUG1:
	case DEBUG2:
	case DEBUG3:
	case DEBUG4:
	case DEBUG5:
		s = gettext_noop("DEBUG");
		break;
	case LOG:
	case LOG_SERVER_ONLY:
		s = gettext_noop("LOG");
		break;
	case INFO:
		s = gettext_noop("INFO");
		break;
	case NOTICE:
		s = gettext_noop("NOTICE");
		break;
	case WARNING:
	case WARNING_CLIENT_ONLY:
		s = gettext_noop("WARNING");
		break;
	case ERROR:
		s = gettext_noop("ERROR");
		break;
	case FATAL:
		s = gettext_noop("FATAL");
		break;
	case PANIC:
		s = gettext_noop("PANIC");
		break;
	default:
		s = "???";
		break;
	}

	return s;
}

PG_FUNCTION_INFO_V1(make_logs);

Datum
make_logs(PG_FUNCTION_ARGS)
{
	ereport(LOG, errcode(ERRCODE_UNDEFINED_CURSOR), errmsg("Lorem ipsum"));
	ereport(LOG, errmsg("Lorem ipsum"), errhint("Class aptent taciti sociosqu ad litora torquent"));
	PG_RETURN_NULL();
}
