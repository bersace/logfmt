# logfmt for PostgreSQL

Format PostgreSQL logs as [logfmt](https://brandur.org/logfmt).

logfmt is a log style designed by Heroku.
It's a compact way of presenting untyped structured log, easier for human to read and for machine to marshal.
logfmt is now a best practice in Grafana/Loki and kubernetes world.

```
ts=2023-05-25T10:01:50.992+0200 level=LOG message="checkpoint starting: time"
ts=2023-05-25T10:01:50.992+0200 level=LOG message="checkpoint complete: wrote 15 buffers (0.1%); 0 WAL file(s) added, 0 removed, 0 recycled; write=1.215 s, sync=0.008 s, total=1.236 s; sync files=13, longest=0.005 s, average=0.001 s; distance=63 kB, estimate=63 kB; lsn=0/1CB7BA0, redo lsn=0/1CB7B68"
```


## Build and Installation from Source

For now, logfmt is only available through git.

- Get the source code using git.
- Setup your PATH with `pg_config` for the target PostgreSQL version.
- Type `make` to build.
- Type `make install` to install.


## Enable logfmt

- Add `logfmt` to `shared_preload_libraries`.
- Restart PostgreSQL.


## About

logfmt is a PostgreSQL extension initiated by Dalibo.
