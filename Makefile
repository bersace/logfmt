EXTENSION = logfmt
MODULE_big = logfmt
DATA = $(wildcard $(EXTENSION)--*.sql)
OBJS = logfmt.o

PG_CONFIG ?= pg_config
PGXS = $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
